#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <Servo.h>
LiquidCrystal_I2C lcd (0x27,16,2);
int button_1 = 4;
int button_2 = 8;
int button_1_value;
int ledPin = 13;
int ledPin_2 = 5;
boolean b_value = 1;
boolean b2_value = 1;
boolean change = 1;
int change2 = 0;
static int case_1 = 1;
int mode = 0;
int MQ2 = A0;
int tonepin = 3;
Servo myservo;
Servo myservo_window;
int pos = 180; 
int pos_1 = 0;
int value = 0;
int window_counter = 0;
#define AUTOMAT 1
#define MANUAL 2
#define PRESSED 1
#define NOPRESS 0
#define LONGPRESS 2
const unsigned long pressTime = 3000;
unsigned long buttonPressStartTime_Navigate = 0; 
int buttonState_Navigate = 0;
unsigned long buttonPressStartTime_Select = 0; 
int buttonState_Select = 0;
int menuState = 0;
int state = 0;
int stateMenu1 = 0;
int stateMenu2 = 0;
int lastState_Navigate;
int lastState_Select;
int val = 0;
void setup ()
{

LCD_start();
pinMode (ledPin, OUTPUT);
pinMode (ledPin_2, OUTPUT);
pinMode(button_1,INPUT_PULLUP);
pinMode(button_2,INPUT_PULLUP);
pinMode (7, OUTPUT);
pinMode (6, OUTPUT);
myservo.attach (9);
myservo_window.attach(10);
pinMode (tonepin, OUTPUT);
pinMode (MQ2, INPUT);
myservo_window.write (30);
Serial.begin(9600);

lcd.clear();
}

void loop ()
{
CheckStateOfButton_Select();
CheckStateOfButton_Navigate();
SelectState();
lastState_Navigate = buttonState_Navigate;
lastState_Select = buttonState_Select;
}

void LCD_start()
{
  lcd.init();
  lcd.backlight();
}

int CheckStateOfButton_Navigate()
{
    button_1_value = digitalRead (button_1);
    delay(50);
    if(button_1_value == LOW)
    {
        if(buttonPressStartTime_Navigate == 0)
        {
          buttonPressStartTime_Navigate = millis();
          while(button_1_value == digitalRead (button_1))
          {
           if(millis() - buttonPressStartTime_Navigate >= pressTime)
           {
              buttonState_Navigate = LONGPRESS;
              break;
           }
           else
           {
            buttonState_Navigate = PRESSED;
           }
          }
        }
    }
    else
    {
      buttonPressStartTime_Navigate = 0;
      buttonState_Navigate = NOPRESS;
    }
    return buttonState_Navigate;
}

int CheckStateOfButton_Select()
{
    button_1_value = digitalRead (button_2);
    delay(50);
    if(button_1_value == LOW)
    {
      buttonState_Select = PRESSED;
    }
    else
    {
      buttonState_Select = NOPRESS;
    }
    return buttonState_Select;
}

int SelectState()
{
  switch(menuState)
  {
    case 0:    
    if(state==0)
    {
    lcd.setCursor (0,0);
    lcd.print("->Automat");
    lcd.setCursor (2,1);
    lcd.print("Manual");
    }
    else if(state == 1)
    {
       lcd.setCursor (2,0);
        lcd.print("Automat");
        lcd.setCursor (0,1);
       lcd.print("->Manual");
    }
    if(state > 1)
    {
      state = 0;
    }
    if(buttonState_Select == PRESSED)
    {
      if(state == 0)
      {
        lcd.clear();
        menuState = AUTOMAT;
        buttonState_Select = NOPRESS;
      }
      else if(state == 1)
      {
        lcd.clear();
        menuState = MANUAL;
      }
    }
    check_navigate_pressed();
    break;

    case 1:
    check_movement();
    check_gas();
    check_soil();
    check_humidity();
    check_darkness();
    check_navigate_longpress();

    break;
    
    case 2:
    check_menu_state_manual();
    check_select_pressed();
    check_navigate_longpress();
    check_navigate_pressed();
    break;
    
    case 3:
    if(stateMenu2==0)
    {
      lcd.setCursor (0,0);
      lcd.print("LED OFF");
      digitalWrite (ledPin, LOW);
    }
    else if(stateMenu2 == 1)
    {
       lcd.setCursor (0,0);
       lcd.print("LED ON");
       digitalWrite (ledPin, HIGH);
    }

    check_navigate_pressed();
  if(stateMenu2 > 1)
  {
    stateMenu2 = 0;
  }
    check_navigate_longpress();
    break;
    
    case 4:
      if(stateMenu2==0)
    {
  lcd.setCursor (0,0);
    lcd.print("LED OFF");
    digitalWrite (ledPin_2, LOW);
    }
    else if(stateMenu2 == 1)
    {
       lcd.setCursor (0,0);
        lcd.print("LED ON");
       digitalWrite (ledPin_2, HIGH);
    }

    check_navigate_pressed();
  if(stateMenu2 > 1)
  {
    stateMenu2 = 0;
  }
    check_navigate_longpress();
    break;
    
    case 5:
    if(stateMenu2==0)
    {
    lcd.setCursor (0,0);
    lcd.print("Fan off");
    digitalWrite (7, LOW);
    digitalWrite (6, LOW);
    }
    else if(stateMenu2 == 1)
    {
      lcd.setCursor (0,0);
      lcd.print("Fan on reverse");
      digitalWrite (7, LOW);
      digitalWrite (6, HIGH);
    }
    else if(stateMenu2 == 2)
    {

       lcd.setCursor (0,0);
       lcd.print("Fan on forward");
       digitalWrite (7, HIGH);
       digitalWrite (6, LOW);
    }

    check_navigate_pressed();
  if(stateMenu2 > 2)
  {
    stateMenu2 = 0;
  }
    check_navigate_longpress();
    break;
    
    case 6:
    val = analogRead (A1);
    val = map(val, 0, 1045, 0, 100);
    lcd.clear();
    lcd.setCursor (0,0);
    lcd.print("Darkness: ");
    lcd.setCursor (0,1);
    lcd.print(val);
    if(val<10)
    {
      lcd.setCursor (1,1);
      lcd.print("%");
    }
    else if(val<100)
    {
      lcd.setCursor (2,1);
    lcd.print("%");
    }
    else
    {
    lcd.setCursor (3,1);
    lcd.print("%");
    }
    delay(100);
    check_navigate_pressed();
  if(stateMenu2 > 2)
  {
    stateMenu2 = 0;
  }
    check_navigate_longpress();
    break;

    case 7:
    val = analogRead (3);
    val = map(val, 0, 405, 0, 100);
    lcd.clear();
    lcd.setCursor (0,0);
    lcd.print("Humidity is: ");
    lcd.setCursor (0,1);
    lcd.print(val);
    if(val<10)
    {
      lcd.setCursor (1,1);
      lcd.print("%");
    }
    else if(val<100)
    {
      lcd.setCursor (2,1);
    lcd.print("%");
    }
    else
    {
    lcd.setCursor (3,1);
    lcd.print("%");
    }
    
   if(val <= 30)
   {
     lcd.setCursor (5,1);
     lcd.print("LOW");
   }
   else if(val >= 70)
   {
     lcd.setCursor (5,1);
     lcd.print("HIGH");
   }
   else
   { 
     lcd.setCursor (5,1);
      lcd.print("NORMAL");
    }
    check_navigate_pressed();
  if(stateMenu2 > 2)
  {
    stateMenu2 = 0;
  }

    check_navigate_longpress();
    delay(100);
    break;

    case 8:
    val = digitalRead (2);
    lcd.clear();
   if(val == 1)
   {
     lcd.setCursor(0,0);
     lcd.print("Movement:");
     lcd.setCursor(2,1);
     lcd.print("Detected");
   }
   else
   { 
     lcd.setCursor(0,0);
     lcd.print("Movement:");
     lcd.setCursor(2,1);
     lcd.print("Not detected");
    }

    check_navigate_pressed();
  if(stateMenu2 > 2)
  {
    stateMenu2 = 0;
  }
    check_navigate_longpress();
    delay(100);
    break;

    case 9:
    lcd.clear();
    val = analogRead (A2);
    val = map(val, 0, 1023, 0, 100);
    lcd.setCursor (0,0);
    lcd.print("Soil humidity: ");
    lcd.setCursor(3,1);
    lcd.print(val);
    lcd.setCursor(5,1);
    lcd.print("%");
    check_navigate_pressed();
    delay(50);

  if(stateMenu2 > 2)
  {
    stateMenu2 = 0;
  }
    check_navigate_longpress();
    delay(100);
    break;

    case 10:
    if(stateMenu2==0)
    {
      lcd.setCursor (0,0);
      lcd.print("Door closed");
      for(; pos_1 < 180; pos_1 += 1)
      {
        myservo.write (pos_1);
        delay (15);
      }
    }
    else if(stateMenu2 == 1)
    {
       lcd.setCursor (0,0);
       lcd.print("Door open");
     for(; pos_1>=100; pos_1-=1)
      {
        myservo.write (pos_1);
        delay (15);
      }
    }
    check_navigate_pressed();
  if(stateMenu2 > 1)
  {
    stateMenu2 = 0;
  }
    check_navigate_longpress();
    break;

    case 11:
    if(stateMenu2==0)
    {
      lcd.setCursor (0,0);
      lcd.print("Window open");     
      myservo_window.write (180);
    }
    else if(stateMenu2 == 1)
    {
       lcd.setCursor (0,0);
       lcd.print("Window closed");
       myservo_window.write (30);
    }
    check_navigate_pressed();
  if(stateMenu2 > 1)
  {
    stateMenu2 = 0;
  }
    check_navigate_longpress();
    break;

    case 12:
       lcd.setCursor (0,0);
       lcd.print("Buzzer on");
      for (int i = 0; i <80; i ++)
         {
           digitalWrite (tonepin, HIGH);
           delay (1);
           digitalWrite (tonepin, LOW);
           delay (1);
         }
    check_navigate_longpress();
    break;

    case 13:
     val = analogRead (MQ2);
  if(val>450)
  {
    lcd.setCursor (0,0);
    lcd.print("Gas detected!!!");
  }
  else
  {
  lcd.setCursor (0,0);  
  lcd.print("Gas not detected");
  }
    check_navigate_longpress();
    break;

    default:
    break;
  }
}

void check_darkness()
{
  val = analogRead (A1);
  val = map(val, 0, 1045, 0, 255);
  if(200 - val < 0)
  {
    analogWrite (ledPin_2, 0);
  }
  else
  {
   analogWrite (ledPin_2, 200 - val);
  }
  delay (5);
}

void check_humidity()
{
  val = analogRead (3);
  val = map(val, 0, 405, 0, 255);
  if(val >= 70)
  {
    myservo_window.write (30);
  }
}

void check_movement()
{
  val = digitalRead (2);
  if(val == 1)
  {
    digitalWrite (ledPin, HIGH);
  }
  else
  {
    digitalWrite (ledPin, LOW);
  }
}

void check_gas()
{
  val = analogRead (MQ2);
  if(val>350)
  {
    
   for(int i = 0; i <80; i ++)
    {
     digitalWrite (tonepin, HIGH);
     delay (1);
     digitalWrite (tonepin, LOW);
     delay (1);
    }
   digitalWrite (7, LOW);
   digitalWrite (6, HIGH);
   myservo_window.write (180);
   digitalWrite(ledPin,HIGH);
   delay(5);
   digitalWrite(ledPin,LOW);
   
   window_counter = 1;
  }
  else
  {
   digitalWrite (6, LOW);
   if(window_counter==1)
   {
    myservo_window.write (30);
   }
   window_counter = 0;
  }
}

void check_soil()
{
  val = analogRead (A2);
  val = map(val, 0, 1023, 0, 100);
  lcd.clear();
  if(val <= 30)
  {
    if(window_counter==1)
    {
      lcd.setCursor (0,0);
      lcd.print("GAS ALERT!!!");
      lcd.setCursor (0,1);
      lcd.print("Soil is dry!");
    }
    else
    {
    lcd.setCursor (0,0);
    lcd.print("Soil is dry!");
    }
  }
  else if(val >= 70)
  {
    if(window_counter==1)
    {
      lcd.setCursor (0,0);
      lcd.print("GAS ALERT!!!");
      lcd.setCursor (0,1);
      lcd.print("Soil is dry!");
    }
    else
    {
    lcd.setCursor (0,0);
    lcd.print("Soil under water!!");
    }
  }
  else
  {
    lcd.clear();
     if(window_counter==1)
    {
      lcd.setCursor (0,0);
      lcd.print("GAS ALERT!!!");
      lcd.setCursor (0,1);
      lcd.print("Soil is dry!");
    }
  }
  delay(50);
}

void check_menu_state_manual()
{
  switch(stateMenu1)
  {
    case 0:
    lcd.setCursor (0,0);
    lcd.print("->LedOutside");
    lcd.setCursor (2,1);
    lcd.print("LedInside");
    break;

    case 1:
    lcd.setCursor (2,0);
    lcd.print("LedOutside");
    lcd.setCursor (0,1);
    lcd.print("->LedInside");
    break;

    case 2:
    lcd.setCursor (0,0);
    lcd.print("->FanModule");
    lcd.setCursor (2,1);
    lcd.print("PhotocellSens");
    break;

    case 3:
    lcd.setCursor (2,0);
    lcd.print("FanModule");
    lcd.setCursor (0,1);
    lcd.print("->PhotocellSens");
    break;

    case 4:
    lcd.setCursor (0,0);
    lcd.print("->AirHumidity");
    lcd.setCursor (2,1);
    lcd.print("SensMovement");
    break;

    case 5:
    lcd.setCursor (2,0);
    lcd.print("AirHumidity");
    lcd.setCursor (0,1);
    lcd.print("->SensMovement");
    break;

    case 6:
    lcd.setCursor (0,0);
    lcd.print("->SoilHumidity");
    lcd.setCursor (2,1);
    lcd.print("Door");
    break;

    case 7:
    lcd.setCursor (2,0);
    lcd.print("SoilHumidity");
    lcd.setCursor (0,1);
    lcd.print("->Door");
    break;

    case 8:
    lcd.setCursor (0,0);
    lcd.print("->Window");
    lcd.setCursor (2,1);
    lcd.print("Buzzer");
    break;

    case 9:
    lcd.setCursor (2,0);
    lcd.print("Window");
    lcd.setCursor (0,1);
    lcd.print("->Buzzer");
    break;

    case 10:
    lcd.setCursor (0,0);
    lcd.print("->GasSensor");
    lcd.setCursor (2,1);
    lcd.print("-----");
    break;

     default:
       break;
  }
}

void check_select_pressed()
{
  if(buttonState_Select == PRESSED && lastState_Select != buttonState_Select)
    {
      lcd.clear();
      switch(stateMenu1)
      {
       case 0: 
       menuState = 3;
       break;

       case 1:
       menuState = 4;
       break;

       case 2:
       menuState = 5;
       break;

       case 3:
       menuState = 6;
       break;

       case 4:
       menuState = 7;
       break;

       case 5:
       menuState = 8;
       break;

       case 6:
       menuState = 9;
       break;

       case 7:
       menuState = 10;
       break;

       case 8:
       menuState = 11;
       break;

       case 9:
       menuState = 12;
       break;

       case 10:
       menuState = 13;
       break;

       default:
       stateMenu1 = 0;
       break;
      }
    }
}
void check_navigate_pressed()
{
  if(buttonState_Navigate == PRESSED && lastState_Navigate != buttonState_Navigate)
     {
       lcd.clear();
       switch(menuState)
       {
         case 0:
         state++;
         break;
        
         case 2:
         stateMenu1++;
         break;

         default:
         stateMenu2++;
         break;
       }
       if(stateMenu1 > 10)
       {
       stateMenu1 = 0;
       }       
     }
}

void check_navigate_longpress()
{
  if(buttonState_Navigate == LONGPRESS && lastState_Navigate != buttonState_Navigate)
    {
      lcd.clear();
      switch(menuState)
      { 
        case 1:
        menuState = 0;
        stateMenu2 = 0;
        break;
        
        case 2:
        menuState = 0;
        stateMenu1 = 0;
        break;

        case 3:
        menuState = 2;
        stateMenu2 = 0;
        break;

        case 4:
        menuState = 2;
        stateMenu2 = 0;
        break;

        case 5:
        menuState = 2;
        stateMenu2 = 0;
        break;

        case 6:
        menuState = 2;
        stateMenu2 = 0;
        break;

        case 7:
        menuState = 2;
        stateMenu2 = 0;
        break;

        case 8:
        menuState = 2;
        stateMenu2 = 0;
        break;

        case 9:
        menuState = 2;
        stateMenu2 = 0;
        break;

        case 10:
        menuState = 2;
        stateMenu2 = 0;
        break;

        case 11:        
        menuState = 2;
        stateMenu2 = 0;
        
        case 12:
        menuState = 2;
        stateMenu2 = 0;
        
        case 13:
        menuState = 2;
        stateMenu2 = 0;
        break;
        
        default:
        lcd.clear();
        break;
      }
    }
}
